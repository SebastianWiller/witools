VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AccessDb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements IFileDatabase

Private c_Db As Access.Application

Private Sub Class_Initialize()
    Set c_Db = New Access.Application
End Sub

Private Sub Class_Terminate()
    c_Db.CloseCurrentDatabase
End Sub

Private Property Let IFileDatabase_Connect(p_Path As String)
    c_Db.OpenCurrentDatabase (p_Path)
End Property

'http://stackoverflow.com/questions/10374286/call-access-sub-from-outlook-vba-method-run-of-object-application-failed
'Verweis: Microsoft Access 15.0 Object Library
Private Property Let IFileDatabase_writeToDatabase(p_SQL As String)
    If Len(p_SQL) = 0 Then
        Exit Property
    End If
    c_Db.CurrentDb.Execute p_SQL
End Property
