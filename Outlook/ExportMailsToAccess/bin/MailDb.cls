VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "MailDb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const C_TABLE_PREFIX = "IMPORT_EMAIL"
Private Const C_COLRECTIME = "M_TIME"
Private Const C_COLSUBJECT = "M_SUBJECT"
Private Const C_COLBODY = "M_BODY"

Private c_Time() As String
Private c_Subject() As String
Private c_Body() As String

Private Sub Class_Initialize()
    ReDim c_Time(0)
    ReDim c_Subject(0)
    ReDim c_Body(0)
End Sub

Public Property Let Add(p_RecieveTime As String, p_Subject As String, p_Body As String)
    'Zeichen " & ' werden entfernt

    ReDim Preserve c_Time(UBound(c_Time) + 1)
    c_Time(UBound(c_Time) - 1) = p_RecieveTime
    
    ReDim Preserve c_Subject(UBound(c_Subject) + 1)
    c_Subject(UBound(c_Subject) - 1) = Replace(Replace(p_Subject, "'", ""), Chr(34), "")
    
    ReDim Preserve c_Body(UBound(c_Body) + 1)
    c_Body(UBound(c_Body) - 1) = Replace(Replace(p_Body, "'", ""), Chr(34), "")
End Property

Public Property Let SaveAtAccessDb(p_DbPath As String)
    Dim Db As IFileDatabase
    Dim strSQL As String
    Dim strTABLE As String
    Dim TableName As String
    Dim i As Integer
    
    Set Db = New AccessDb
    With Db
        .Connect = p_DbPath
    
        TableName = createTable(Db)
        
        strSQL = ""
        For i = 0 To UBound(c_Time) - 1
            strSQL = "INSERT INTO " & TableName & " VALUES (" & _
                "'" & c_Time(i) & "'," & _
                "'" & c_Subject(i) & "'," & _
                "'" & c_Body(i) & "'" & _
                ");"
            .writeToDatabase = strSQL
        Next i
    End With
    Set Db = Nothing
End Property

Private Function createTable(p_db As IFileDatabase) As String
    Dim returnValue As String
    Dim strSQL As String
    
    returnValue = C_TABLE_PREFIX & Format(Now(), "YYYYMMDD")
    
    If Not Access.Application.CurrentDb.OpenRecordset("SELECT Name FROM MSysObjects WHERE Type = 1 AND Name='" & returnValue & "'").RecordCount = 1 Then
        strSQL = "CREATE TABLE " & returnValue & " (" & _
            C_COLRECTIME & " TEXT(23)," & _
            C_COLSUBJECT & " TEXT(255)," & _
            C_COLBODY & " MEMO" & _
            ");"
        p_db.writeToDatabase = strSQL
    End If
    
    createTable = returnValue
End Function
