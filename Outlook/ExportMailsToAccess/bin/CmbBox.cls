VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CmbBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private c_Form As Form_CmbBox

Private Sub Class_Initialize()
    Set c_Form = New Form_CmbBox
    With c_Form
        .ComboBoxValues.RowSource = ""
        .Caption = "W�hlen Sie einen Wert"
    End With
End Sub

Private Sub Class_Terminate()

End Sub

Public Property Let Caption(p_Titel As String)
    c_Form.Caption = p_Titel
End Property

Public Property Let Values(p_Values() As String)
    Dim i As Integer
    Dim str As String
    'Sort Array
    If UBound(p_Values) > 1 Then
        For i = 0 To UBound(p_Values) - 1
            If p_Values(i) > p_Values(i + 1) Then
                str = p_Values(i)
                p_Values(i) = p_Values(i + 1)
                p_Values(i + 1) = str
                i = -1
            End If
        Next i
    End If
    
    With c_Form.ComboBoxValues
        For i = 0 To UBound(p_Values) - 1
            .AddItem p_Values(i)
        Next i
    End With
End Property

Public Property Get Show() As String
    Dim returnValue As String
    
    returnValue = ""
    
    c_Form.Show
    returnValue = c_Form.ComboBoxValues.Text
    
    Show = returnValue
End Property
