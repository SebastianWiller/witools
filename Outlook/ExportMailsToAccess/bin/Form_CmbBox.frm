VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} Form_CmbBox 
   Caption         =   "UserForm1"
   ClientHeight    =   1215
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   OleObjectBlob   =   "Form_CmbBox.frx":0000
   StartUpPosition =   1  'Fenstermitte
End
Attribute VB_Name = "Form_CmbBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub ButtonAbort_Click()
    Me.ComboBoxValues.Text = ""
    Unload Me
End Sub

Private Sub ButtonOK_Click()
    Unload Me
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)
    If CloseMode = 0 Then
        Me.ComboBoxValues.Text = ""
    End If
End Sub
