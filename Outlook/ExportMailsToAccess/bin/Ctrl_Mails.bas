Attribute VB_Name = "Ctrl_Mails"
Option Explicit

'Quelle: http://stackoverflow.com/questions/2272361/can-i-iterate-through-all-outlook-emails-in-a-folder-including-sub-folders
'Verweis: Microsoft XML, v3.0 (MSXML2)
Private Function getMails(p_Folder As String) As String()
    Dim oInbox    As Outlook.Folder
    Dim oItem     As Object
    Dim oMailItem As MailItem
    Dim oMailBody As String
    Dim MailsDb As MailDb
    Dim rValue As Integer
    Dim returnValue(0) As String
    
    Set oInbox = ActiveExplorer.Session.DefaultStore.GetRootFolder().Folders(p_Folder)
    Set MailsDb = New MailDb
    
    For Each oItem In oInbox.Items
        If TypeOf oItem Is MailItem Then
            Set oMailItem = oItem
            oMailBody = getMailBody(oMailItem)
            'MsgBox oMailItem.ReceivedTime & Chr(10) & oMailItem.Subject & Chr(10) & Left(oMailBody, 50)
            MailsDb.Add(oMailItem.ReceivedTime, oMailItem.Subject) = oMailBody
            rValue = MsgBox("Weiter?", vbYesNo)
            If rValue = vbNo Then
                GoTo PointToReturn
            End If
        Else
            Debug.Print "Skipping " & TypeName(oItem)
        End If
    Next
    
PointToReturn:
    MailsDb.SaveAtAccessDb = "C:\Users\sebas\Documents\Datenbank1.accdb"
    getMails = returnValue
End Function

Private Function getFolders(p_IgnoreFolders() As String) As String()
    Dim returnValue() As String
    Dim oFolder As Outlook.Folder
    Dim i As Integer
    
    ReDim returnValue(0)
    
    For Each oFolder In ActiveExplorer.Session.DefaultStore.GetRootFolder().Folders
        If oFolder.DefaultItemType = 0 Then 'Email-Ordner
            For i = 0 To UBound(p_IgnoreFolders) - 1
                If oFolder.Name = p_IgnoreFolders(i) Then
                    GoTo NextFolder
                End If
            Next i
            
            ReDim Preserve returnValue(UBound(returnValue) + 1)
            returnValue(UBound(returnValue) - 1) = oFolder.Name
        End If
NextFolder:
    Next
    
    getFolders = returnValue
End Function

'Quelle: http://stackoverflow.com/questions/2272361/can-i-iterate-through-all-outlook-emails-in-a-folder-including-sub-folders
Private Function getMailBody(ByVal MailItem As MailItem) As String
    Dim returnValue As String

    returnValue = ""

    Select Case MailItem.BodyFormat
        Case OlBodyFormat.olFormatPlain, OlBodyFormat.olFormatUnspecified
            returnValue = MailItem.Body
        Case OlBodyFormat.olFormatHTML
            returnValue = MailItem.HTMLBody
        Case OlBodyFormat.olFormatRichText
            returnValue = MailItem.RTFBody
    End Select
    
    getMailBody = returnValue
End Function

Public Sub Test()
    Dim Ordner() As String
    Dim OrdnerToIgnore() As String
    Dim strSelected As String
    Dim i As Integer
    Dim Box As CmbBox
    
    ReDim OrdnerToIgnore(5)
    OrdnerToIgnore(0) = "Synchronisierungsprobleme (Nur dieser Computer)"
    OrdnerToIgnore(1) = "RSS-Feeds"
    OrdnerToIgnore(2) = "Einstellungen f�r Unterhaltungsaktionen"
    OrdnerToIgnore(3) = "Einstellungen f�r QuickSteps"
    OrdnerToIgnore(4) = "Entw�rfe (Nur dieser Computer)"
    
    Ordner = getFolders(OrdnerToIgnore)
    
    Set Box = New CmbBox
    With Box
        .Values = Ordner
        strSelected = .Show
    If Len(strSelected) <> 0 Then
        getMails strSelected
    End If
End Sub

